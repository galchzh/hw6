import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  register(email:string, password:string)
  {
    return this.fireBaseAuth
              .auth
              .createUserWithEmailAndPassword(email, password);
  }

  updateProfile(user, name:string)
  {
    user.updateProfile({displayName:name, photoURL:''});
  }

  isAuth()
  {
    return this.fireBaseAuth.authState.pipe(map(auth => auth));
  }

  
 
  constructor(private fireBaseAuth: AngularFireAuth) 
  {

  }
  
}
